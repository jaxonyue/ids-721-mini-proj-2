# IDS 721 Mini Proj 2 [![pipeline status](https://gitlab.com/jaxonyue/ids-721-mini-proj-2/badges/main/pipeline.svg)](https://gitlab.com/jaxonyue/ids-721-mini-proj-2/-/commits/main)

## Overview
* This repository includes the components for **Mini-Project 2 - Create a simple AWS Lambda function that processes data**

## Goal
* Implement a simple AWS Lambda Function using Cargo Lambda
* The Lambda Function processes data and returns a response
* Integrate API Gateway to trigger the Lambda Function

## My Lambda Function
* My script is written in Rust and uses the Cargo Lambda framework. It contains two functions:
  * `uppercase` - This function takes a string as input and returns the string in uppercase
  * `lowercase` - This function takes a string as input and returns the string in lowercase

## Key Steps
1. Install Rust and Cargo Lambda per the instructions in the [Cargo Lambda documentation](https://www.cargo-lambda.info/guide/installation.html) and [Rust documentation](https://www.rust-lang.org/tools/install)
2. Create a new Rust project using `cargo lambda new <project_name>`
3. Write the code for the Lambda Function in the `src/main.rs` file
4. Test the Lambda Function by first `cargo lambda watch` to build the project and then `cargo lambda invoke <function_name> --event <event_file>.json` to test the function
5. Sign into AWS and go to the IAM console, add a new user, choose "Attch policies directly" and select "IAMFullAccess" and "AWSLambda_FullAccess"
6. Finish creating the user, navigate to the "Security Credentials" tab, and create an access key
7. Store AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, and AWS_REGION in a newly created `.env` file
8. Create a `.gitignore` file and add `.env` to it to prevent the file from being pushed to Gitlab
9. In the terminal, run the following commands to set the environment variables
```
set -a # automatically export all variables
source .env
set +a
```
10. Run `cargo lambda deploy` to deploy the Lambda Function to AWS
11. Go to the AWS Lambda console and test the function by creating a new test event using the follwing event JSON:
```
{
  "command": "<insert command>"
  "data": "<insert data>"
}
```
12. Add a new trigger to the Lambda Function by selecting "API Gateway", creating a new API, REST API, and "Open" security
13. Deploy the API and test the function using the API Gateway URL using the following format:
```
curl -X POST -H "Content-Type: application/json" -d '{"command": "<insert command>", "data": "<insert data>"}' <API Gateway URL>
```

## Lambda Function Diagram
![Screenshot_2024-02-03_at_4.03.18_PM](/uploads/a9ede24c327a879dc98b2accb358ab7b/Screenshot_2024-02-03_at_4.03.18_PM.png)

## Successful API Call Screenshot
The first call is for the `uppercase` function and the second call is for the `lowercase` function
![Screenshot_2024-02-03_at_4.04.15_PM](/uploads/d366c970979144e699d54fae297540ef/Screenshot_2024-02-03_at_4.04.15_PM.png)