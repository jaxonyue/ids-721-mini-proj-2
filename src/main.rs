use lambda_runtime::{run, service_fn, Error, LambdaEvent};
use serde::{Deserialize, Serialize};
use tracing_subscriber::filter::{EnvFilter, LevelFilter};
use anyhow::anyhow;

/// Request structure expects a command ("uppercase" or "lowercase") and a message to convert.
#[derive(Deserialize)]
struct Request {
    command: String,
    message: String,
}

/// Response structure to send back a transformed message.
#[derive(Serialize)]
struct Response {
    transformed_message: String,
}

/// The main handler for the Lambda function.
async fn function_handler(event: LambdaEvent<Request>) -> Result<Response, Error> {
    let (event, _context) = event.into_parts();
    let transformed_message = match event.command.as_str() {
        "uppercase" => to_uppercase(&event.message),
        "lowercase" => to_lowercase(&event.message),
        _ => return Err(anyhow!("Invalid command").into()),
    };

    Ok(Response { transformed_message })
}

/// Converts an input string to all uppercase letters.
fn to_uppercase(input: &str) -> String {
    input.to_uppercase()
}

/// Converts an input string to all lowercase letters.
fn to_lowercase(input: &str) -> String {
    input.to_lowercase()
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_env_filter(
            EnvFilter::builder()
                .with_default_directive(LevelFilter::INFO.into())
                .from_env_lossy(),
        )
        // disable printing the name of the module in every log line.
        .with_target(false)
        // disabling time is handy because CloudWatch will add the ingestion time.
        .without_time()
        .init();

    run(service_fn(function_handler)).await
}
